FactoryGirl.define do
  factory :user do
    sequence :email do |n|
      "test#{n}@example.com"
    end
    password "please123"

    trait :admin do
      role 'admin'
    end

  end
end
